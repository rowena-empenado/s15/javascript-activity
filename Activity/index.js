/* 
	Console log the message Hello World to ensure that the script file is properly associated with the html file.
*/

	console.log("Hello World");

/* 
	Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:

		- If the total of the two numbers is less than 10, add the numbers
		- If the total of the two numbers is 10 - 19, subtract the numbers
		- If the total of the two numbers is 20 - 29 multiply the numbers
		- If the total of the two numbers is greater than or equal to 30, divide the numbers

	Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
*/

let num1 = parseInt(prompt("Enter first number:"));
let num2 = parseInt(prompt("Enter second number:"));

let sum = (num1 + num2);
let difference = (num1 > num2) ? (num1 - num2) : (num2 - num1);
let product = num1 * num2;
let quotient = (num1 > num2) ? (num1 / num2) : (num2 / num1);

function performArithmeticOperation(num1, num2) {
	
	if (sum < 10) {
		console.warn("The result is: " + sum + " (SUM)");
	} else if (sum >= 10 && sum <=19) {
		alert("The result is: " + difference + " (DIFFERENCE)");
	} else if (sum >= 20 && sum <=29) {
		alert("The result is: " + product + " (PRODUCT)");
	} else if (sum >= 30) {
		alert("The result is: " + quotient + " (QUOTIENT)");
	}
}
performArithmeticOperation(num1, num2);

	
/* 
	Prompt the user for their name and age and print out different alert messages based on the user input:
	-  If the name OR age is blank/null, print the message are you a time traveler?
	-  If the name AND age is not blank, print the message with the user’s name and age.
*/

let name = prompt("Enter your name:");
let age = prompt("Enter your age:");

function printMessage(name, age) {
	if ((name == "") || (age == "")) {
		alert("Are you a time traveler?")
	} else if ((name != "") && (age != "")) {
		alert("Your name is " + name + " and you are " + age + " years old.")
	}
}
printMessage(name, age);


/* 
	Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
	- 18 or greater, print an alert message saying You are of legal age.
	- 17 or less, print an alert message saying You are not allowed here.
*/


function isLegalAge(age) {

	(age >= 18) ? alert("You are of legal age.") : alert("You are not allowed here.")
	
}
isLegalAge(age);


/* 
	Create a switch case statement that will check if the user's age input is within a certain set of expected input:
	- 18 - print the message You are now allowed to party.
	- 21 - print the message You are now part of the adult society.
	- 65 - print the message We thank you for your contribution to society.
	- Any other value - print the message Are you sure you're not an alien?
*/

let ageNumber = parseInt(age);

switch (ageNumber) {
	case 18:
		alert("You are now allowed to party.");
		break;
	case 21:
		alert("You are now part of the adult society.");
		break;
	case 65:
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you're not an alien?")
		break;
}


/* 
	Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.
*/
function tryCatchFinally(ageNumber) {

	try {
		alert(isLegalAge(gfgfgfhbg));
	}

	catch(error) {
		console.warn(error.message)
	}

	finally {
		alert('Your age is: ' + ageNumber + '\n\nThis ends the function.')
	}

}
tryCatchFinally(ageNumber);